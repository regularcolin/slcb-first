# README #

This is a StreamLabs Chatbot Script initially written by Must13.

Must13 no longer appears to stream, but his Twitch page is twitch.tv/Must13

Not confirmed but I think this is his username on Twitter: `@_Must13_`

I have rewritten the script to support any rewarding number of users.
### What is this repository for? ###

* Reward the first N users to tune into your stream with bonus channel points.
* To claim, users type the command "!first".
* Version 1.0.4 (previous versions all by Must13)

### How do I get set up? ###

* Add First_StreamlabsSystem.py and UI.Config to a folder (recommended folder name: 'first')
* Add that folder to a zip file (first.zip)
* In Streamlabs Chatbot, you can now import the script and twiddle the settings to your leisure.


* Example configuration: Reward the first 5 users. Give 100 to the first user, and deduct 10 from that for each subsequent user.
* This would give 100 to 1st place, 90 for 2nd, 80 for 3rd, etc.
* Note: If any rewards would be less than 1 channel point (0 or negative), 1 channel point is granted instead.